cask 'reflector-2' do
  version '2.7.3'
  # shasum -a 256
  sha256 'fd9e4c1ee48d113c09c5e2736001a20e3eff6fdf655ec974b814a190c1c8b76e'

  url "https://download.airsquirrels.com/Reflector#{version.major}/Mac/Reflector-#{version}.dmg"
  appcast "https://updates.airsquirrels.com/Reflector#{version.major}/Mac/Reflector#{version.major}.xml",
          checkpoint: '38ab599bebca2d67ec695ac14440cfb62e9dddefe81b8f4cdf07d931c2851f65'
  name "Reflector #{version.major}"
  homepage 'http://www.airsquirrels.com/reflector/'

  app "Reflector #{version.major}.app"
end