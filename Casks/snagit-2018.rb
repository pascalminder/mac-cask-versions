cask 'snagit-2018' do
  version '2018'
  # Version: 18.2.7
  # Released: 2019 Aug 08
  # https://www.techsmith.com/download/oldversions
  sha256 :no_check # required as upstream package is updated in-place

  url 'https://download.techsmith.com/snagitmac/enu/1827/snagit.dmg'
  name 'Snagit'
  homepage 'https://www.techsmith.com/screen-capture.html'

  app "Snagit #{version}.app"
end